=================================================
Welcome to THPS 2 PC README.TXT
=================================================

Created: April 4, 2001

Warning:
If using a Korean or non-western Keyboard, THPS2 requires that the keyboard must be set to English text characters. This can be done by using the Windows(r) English/Korean toggle key located on your keyboard. 

This document contains useful information for THPS 2 PC.  Please look over this document carefully for any questions that you may have about the game.  If you are looking for further answers, please look at our website at http://www.activision.com.  

For THPS 2 Korean specific credits, please follow the "Credits" link in the technical help document.
 
CONTENTS
I. Installation Instructions
II. Minimum System Requirements
III. Troubleshooting
IV. Keyboard and Gamepad/Joystick Controls
V. Software License Agreement


I. Installation instructions

INSTALLING TONY HAWK'S PRO SKATER 2

- To Install Tony Hawk's Pro Skater 2, insert the game CD into your CD-ROM drive.
- If Autoplay is enabled, the Installer splash screen should appear.
- When the Installer splash screen appears, click on the Install button.
- To view the online manuals, click on the more button.
- Uninstall THPS2 Shortcut: If you wish to remove THPS2 from your hard drive, you should always use the Shortcut from the Start Menu. The Uninstall Tony Hawk's Pro Skater 2 option will remove all game files except your saved games, screen shots and personal settings.

Note: You can return to the splash screen any time without affecting your installed game.

AUTOPLAY

If the THPS2 splash screen does not appear, try performing the following steps:
- Double-click on the My Computer icon on your desktop, or right-click on the icon and choose the Open option.
- Select the Refresh option located in the View Pull-down Menu.
- Double-click on the THPS2 CD icon in the window or right-click on the icon and choose the Autoplay option.
- After the THPS2 splash screen appears, click on the Install button.

If the Autoplay feature does not function, please check the following:
- Make sure the CD is clean and properly placed in the CD-ROM drive.
- Your CD-ROM driver may not be optimized for use with Windows(R) 95/98.

To verify this, perform the following steps:
- Open the Windows(R) 95/98 Control Panel folder and double-click on the System icon.
- Click on the Performance tab. If any of your hardware drivers are not fully optimized for use with Windows(R) 95/98, they will be listed here with an explanation of the exact problem and suggestions on how to fix it.

Windows(R) 95/98 may not be setup to Autoplay CD's. To check, perform the following steps:
- Open the Windows(R) 95/98 Control Panel folder and double-click on the System icon.
- Click on the Device Manager tab. Click on the Plus sign next to CD-ROM, select your CD-ROM and choose Properties.
- Click on the Settings tab. Insert a checkmark in the box to the left of the Auto Insert Notification and select OK.

If Autoplay still does not work, please try the following:
- Double-click on the Setup.exe at the root level of the CD, or
- Go to Run in your Start Menu and type d:\Setup.exe (substituting your CD-ROM's drive letter if different from "d:").

 
II. Minimum System Requirements

-100% DirectX(R) 7.0a compliant 3-D Hardware Accelerator* for hardware acceleration. 
-100% DirectX(R) 7.0a compliant 2-D video card for software mode. 
-A 100% Windows(R) 95/98 compatible computer system (Including compatible 32-bit drivers for CD-
ROM, video card, sound card and input devices)
-233 MHz Pentium(R) Processor (Hardware Mode); P2 266 Pentium(R) Processor (Software Mode)
-375 MB of uncompressed hard disk space to install, plus an additional 95 MB for Windows swap file. 
-32 MB of RAM
-Korean language Version of Windows 95/98 Operating System
-100% DirectX(R) 7.0a or higher compatible sound card
-100% Microsoft(R) compatible mouse and driver
 


*3-DGraphics Accelerator Card 

A 100% DirectX(R) 7.0a or higher compliant video card and driver with z-buffering capability is recommended. 
THPS 2 PC uses Microsoft's(R) Direct 3D to support 3-D hardware acceleration. It has been tested on many, 
but not all, of the major cards incorporating the chipsets listed below. Some 3-D accelerator card brands 
utilizing the chipsets whose names appear below may not be fully compatible with the 3-D accelerator 
features of THPS 2 PC. For a list of the cards and drivers that have been tested, please visit 
http://www.activision.com. 

Supported chipsets for Windows 95/98**

-3DFX Banshee	
-3DFX Voodoo 1		
-3DFX Voodoo 2
-3DFX Voodoo 3	
-3DFX Voodoo 5					
-ATI Rage 128
-GeForce 256				
-Matrox G400				
-nVidia Riva 128
-nVidia TNT		
-nVidia TNT2	
-nVidia TNT2 Ultra
-nVidia GeForce
-nVidia GeForce 2	

**Some, but not all, of the cards with the chipsets listed above have been tested on Windows 2000. For Windows 2000 3D support, please refer to your hardware manufacturer for 100% Windows 2000 DirectX 7.0a compliant drivers.

IMPORTANT NOTE: This product uses the Microsoft's DirectX technology, which requires your system to have the latest Windows 95/98 drivers that fully support DirectX 7.0a.	


III. Troubleshooting

Q - What is software mode and why would I need to run in it?
A - Software mode is for computers that cannot run the game in Hardware mode.  In software mode, the game relies on the computer, instead of the video card, to render the game.  This causes some hits to the performance and the image quality.  To see if your video card supports hardware mode, please check the above list of supported video cards.

Q - How can I navigate the Tony Hawk Pro Skater 2 shell screens?
A - The THPS 2 shell was designed to be navigated with three different input devices:  Joystick/Gamepad, Keyboard, and mouse. However, some shell screens require only the keyboard or gamepad.  The ENTER key on the keyboard or the JUMP button on the gamepad are universally used to change/accept options.

Q - I have a Force Feedback (tm) joystick/gamepad but there are no forces in the game?
A - Make sure you have the latest drivers for your input device and it is configured correctly in Windows.

Q - I have a Keytronic keyboard and I am having trouble maneuvering while pressing three keys at once.
A - There are some documented problems with this keyboard and THPS 2.  At the time of release there was not a fix for this issue.  However, Activision is working to eliminate this issue.

Q - Does Tony Hawk's Pro Skater 2 work with Dual Monitors?
A - A dual monitor setup has been tested with the game and it has been found that it causes problems with the game. It is suggested that if you have a dual monitor setup then simply disable the second monitor prior to playing the game.

Q - When I use the Level-Flip cheat, everything is backwards.
A - That's right, the Level-Flip cheat not only flips the level, but also the skater and the moves.  Have fun!

IV. Keyboard and Gamepad/Joystick Controls

Keyboard
IN SHELL
Previous Screen		Escape
Accept			Enter or Space or Numpad Enter

Set Trucks/Board	Numpad 6  (skateshop)
Goals			Numpad 6  (level select screen)

Up  			Up Arrow
Down  			Down Arrow
Left 			Left Arrow
Right 		 	Right Arrow

Q			Quit

IN GAME
Grind			Numpad 8, V
Jump/Ollie		Numpad 2, SPACEBAR
Grab			Numpad 6, B
Fliptrick (kick)	Numpad 4, C

Up  			Up Arrow or W
Down  			Down Arrow or S
Left 			Left Arrow or A
Right 		 	Right Arrow or D

(during replay)
Slow-Motion		Numpad 5  (The user first has to hit the numpad 4 to pause the game in order for this feature to function)

(in air)
Spin Left Continuous	Numpad 7
Spin Right Continuous	Numpad 9
Spin Left 180		Numpad 1
Spin Right 180		Numpad 3

(on ground)
Nollie/Fakie		Numpad 1
Switch Stance		Numpad 3

Pause			P or Escape


Joystick (default 10-button Gamepad)
Button 1			Fliptrick (kick)
Button 2			Jump/Ollie
Button 3			Grab
Button 4			Grind, Back
Button 5			Spin Left Continuous
Button 6			Spin Right Continuous
Button 7			Nollie/Spin Left 180
Button 8		        Stance/Spin Right 180
Button 9			Camera Change  (Not bound by default)
Button 0			Pause Menu (Not bound by default)

Trick Setup Key:
Directions + S=  Grind/Lip Tricks
Directions + G=  Grab Tricks
Directions + K=  Kick Tricks
		
V.  Software License Agreement

SOFTWARE LICENSE AGREEMENT
IMPORTANT - READ CAREFULLY: USE OF THIS PROGRAM IS SUBJECT TO THE SOFTWARE LICENSE TERMS SET FORTH BELOW. "PROGRAM" INCLUDES THE SOFTWARE INCLUDED WITH THIS AGREEMENT, THE ASSOCIATED MEDIA, ANY PRINTED MATERIALS, AND ANY ON-LINE OR ELECTRONIC DOCUMENTATION, AND ANY AND ALL COPIES AND DERIVATIVE WORKS OF SUCH SOFTWARE AND MATERIALS. BY OPENING THIS PACKAGE, INSTALLING, AND/OR USING THE PROGRAM, YOU ACCEPT THE TERMS OF THIS LICENSE WITH ACTIVISION, INC. ("ACTIVISION"). 
LIMITED USE LICENSE. Activision grants you the non-exclusive, non-transferable, limited right and license to install and use one copy of this Program solely and exclusively for your personal use. All rights not specifically granted under this Agreement are reserved by Activision. This Program is licensed, not sold. Your license confers no title or ownership in this Program and should not be construed as a sale of any rights in this Program.
OWNERSHIP. All title, ownership rights and intellectual property rights in and to this Program and any and all copies thereof (including but not limited to any titles, computer code, themes, objects, characters, character names, stories, dialog, catch phrases, locations, concepts, artwork, animation, sounds, musical compositions, audio-visual effects, methods of operation, moral rights, any related documentation, and "applets" incorporated into this Program) are owned by Activision or its licensors. This Program is protected by the copyright laws of the United States, international copyright treaties and conventions and other laws. This Program contains certain licensed materials and Activision's licensors may protect their rights in the event of any violation of this Agreement.
YOU SHALL NOT:
o Exploit this Program or any of its parts commercially, including but not limited to use at a cyber cafe, computer gaming centre or any other location-based site. Activision may offer a separate Site License Agreement to permit you to make this Program available for commercial use; see the contact information below.
o Use this Program, or permit use of this Program, on more than one computer, computer terminal, or workstation at the same time.
o Make copies of this Program or any part thereof, or make copies of the materials accompanying this Program.
o Copy this Program onto a hard drive or other storage device; you must run this Program from the included CD-ROM (although this Program itself may automatically copy a portion of this Program onto your hard drive during installation in order to run more efficiently).
o Use the program, or permit use of this Program, in a network, multi-user arrangement or remote access arrangement, including any online use, except as otherwise explicitly provided by this Program.
o Sell, rent, lease, license, distribute or otherwise transfer this Program, or any copies of this Program, without the express prior written consent of Activision.
o Reverse engineer, derive source code, modify, decompile, disassemble, or create derivative works of this Program, in whole or in part.
o Remove, disable or circumvent any proprietary notices or labels contained on or within the Program.
o Export or re-export this Program or any copy or adaptation in violation of any applicable laws or regulations. By using this Program you are warranting that you are not a "foreign person," as defined by U.S. government regulations, or under the control of a foreign person.

ACTIVISION Limited 90-Day Warranty
Activision warrants to the original consumer purchaser of this computer software product that the recording medium on which the software program is recorded will be free from defects in material and workmanship for 90 days from the date of purchase. If the recording medium is found defective within 90 days of original purchase, ACTIVISION agrees to replace, free of charge, any product discovered to be defective within such period upon receipt at its Factory Service Centre of the product, postage paid, with proof of date of purchase, as long as the program is still being manufactured by ACTIVISION. In the event that the program is no longer available, ACTIVISION retains the right to substitute a similar product of equal or greater value.
This warranty is limited to the recording medium containing the software program originally provided by ACTIVISION and is not applicable to normal wear and tear. This warranty shall not be applicable and shall be void if the defect has arisen through abuse, mistreatment, or neglect. Any implied warranties applicable to this product are limited to the 90-day period described above.
EXCEPT AS SET FORTH ABOVE, THIS WARRANTY IS IN LIEU OF ALL OTHER WARRANTIES, WHETHER ORAL OR WRITTEN, EXPRESS OR IMPLIED, INCLUDING ANY WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, AND NO OTHER REPRESENTATION OR CLAIMS OF ANY KIND SHALL BE BINDING ON OR OBLIGATE ACTIVISION. IN NO EVENT WILL ACTIVISION BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGE RESULTING FROM POSSESSION, USE OR MALFUNCTION OF THIS PRODUCT, INCLUDING DAMAGE TO PROPERTY AND, TO THE EXTENT PERMITTED BY LAW, DAMAGES FOR PERSONAL INJURY, EVEN IF ACTIVISION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME STATES DO NOT ALLOW LIMITATIONS ON HOW LONG AN IMPLIED WARRANTY LASTS AND/OR THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS AND/OR EXCLUSION OR LIMITATION OF LIABILITY MAY NOT APPLY TO YOU. THIS WARRANTY GIVES YOU SPECIFIC LEGAL RIGHTS, AND YOU MAY HAVE OTHER RIGHTS WHICH VARY FROM STATE TO STATE.
When returning merchandise for replacement please send the original product disks only in protective packaging and include:
1.	A photocopy of your dated sales receipt
2.	Your name and return address, typed or clearly printed
3.	A brief note describing the defect, the problem(s) you encountered and the system on which you are running the product
4.	If you are returning the product after the 90-day warranty period, but within one year after the date of purchase, please include a check or money order for $10 U.S. (AUD $17 for Australia, or 10.00 Pounds for Europe) currency per CD or floppy disk replacement
Note: Certified mail is recommended.

In Europe send to:

WARRANTY REPLACEMENTS
ACTIVISION (UK) Ltd., Parliament House, St Laurence Way, Slough, Berkshire, SL1 2BW, United Kingdom.
Disc Replacement: + (44) 1753 756 189

In Australia send to:

Warranty Replacements
Activision
P.O. Box 873
Epping, NSW 2121, Australia


LIMITATION ON DAMAGES. IN NO EVENT WILL ACTIVISION BE LIABLE FOR SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES RESULTING FROM POSSESSION, USE OR MALFUNCTION OF THE PROGRAM, INCLUDING DAMAGES TO PROPERTY, LOSS OF GOODWILL, COMPUTER FAILURE OR MALFUNCTION AND, TO THE EXTENT PERMITTED BY LAW, DAMAGES FOR PERSONAL INJURIES, EVEN IF ACTIVISION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. ACTIVISION'S LIABILITY SHALL NOT EXCEED THE ACTUAL PRICE PAID FOR THE LICENSE TO USE THIS PROGRAM. SOME STATES/COUNTRIES DO NOT ALLOW LIMITATIONS ON HOW LONG AN IMPLIED WARRANTY LASTS AND/OR THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS AND/OR EXCLUSION OR LIMITATION OF LIABILITY MAY NOT APPLY TO YOU. THIS WARRANTY GIVES YOU SPECIFIC LEGAL RIGHTS, AND YOU MAY HAVE OTHER RIGHTS WHICH VARY FROM JURISDICTION TO JURISDICTION.
TERMINATION. Without prejudice to any other rights of Activision, this Agreement will terminate automatically if you fail to comply with its terms and conditions. In such event, you must destroy all copies of this Program and all of its component parts.
U.S. GOVERNMENT RESTRICTED RIGHTS. The Program and documentation have been developed entirely at private expense and are provided as "Commercial Computer Software" or "restricted computer software." Use, duplication or disclosure by the U.S. Government or a U.S. Government subcontractor is subject to the restrictions set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and Computer Software clauses in DFARS 252.227-7013 or as set forth in subparagraph (c)(1) and (2) of the Commercial Computer Software Restricted Rights clauses at FAR 52.227-19, as applicable. The Contractor/Manufacturer is Activision, Inc., 3100 Ocean Park Boulevard, Santa Monica, California 90405.
INJUNCTION. Because Activision would be irreparably damaged if the terms of this Agreement were not specifically enforced, you agree that Activision shall be entitled, without bond, other security or proof of damages, to appropriate equitable remedies with respect to breaches of this Agreement, in addition to such other remedies as Activision may otherwise have under applicable laws.
INDEMNITY. You agree to indemnify, defend and hold Activision, its partners, affiliates, contractors, officers, directors, employees and agents harmless from all damages, losses and expenses arising directly or indirectly from your acts and omissions to act in using the Product pursuant to the terms of this Agreement 
MISCELLANEOUS. This Agreement represents the complete agreement concerning this license between the parties and supersedes all prior agreements and representations between them. It may be amended only by a writing executed by both parties. If any provision of this Agreement is held to be unenforceable for any reason, such provision shall be reformed only to the extent necessary to make it enforceable and the remaining provisions of this Agreement shall not be affected. This Agreement shall be construed under California law as such law is applied to agreements between California residents entered into and to be performed within California, except as governed by federal law and you consent to the exclusive jurisdiction of the state and federal courts in Los Angeles, California. 
If you have any questions concerning this license, you may contact Activision at 3100 Ocean Park Boulevard, Santa Monica, California 90405, + (310) 255-2000, Attn. Business and Legal Affairs, legal@activision.com.

*Addendum to English Manual:
The correct address for the UK office warranty replacements is:
Activision (UK) Ltd., Parliament House, St Laurence Way, Slough, Berkshire, SL1 2BW, United Kingdom.
Disc Replacement: +(44) 1753 756 189
